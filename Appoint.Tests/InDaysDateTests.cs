﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestPlatform.UnitTestFramework;

using Appoint.Models;

namespace Appoint.Tests
{
    [TestClass]
    public class InDaysDateTests : BaseDateTests
    {
        protected override BaseDate Create()
        {
            return new InDaysDate();
        }

        protected override string Kind
        {
            get { return InDaysDate.Kind; }
        }

        protected override DateTime GetDefault(DateTime date)
        {
            return DateTime.Today.AddDays(10);
        }

        [TestMethod]
        public virtual void YieldsProperDateBasedOnTodayWithoutTime()
        {
            for (int d = 0; d <= 720; ++d)
            {
                var date = Create();
                date.Span = d;

                Assert.AreEqual(DateTime.Today.Date.AddDays(d), date.Date);
                Assert.AreEqual(0, date.Date.TimeOfDay.TotalSeconds);
            }
        }
    }
}
