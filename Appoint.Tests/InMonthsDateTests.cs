﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;
using Microsoft.VisualStudio.TestPlatform.UnitTestFramework;

using Appoint.Models;

namespace Appoint.Tests
{
    [TestClass]
    public class InMonthsDateTests : BaseDateTests
    {
        protected override BaseDate Create()
        {
            return new InMonthsDate();
        }

        protected override string Kind
        {
            get { return AppointmentFactory.Kinds.InMonths; }
        }

        protected override DateTime GetDefault(DateTime date)
        {
            return date.AddMonths(2);
        }

        [TestMethod]
        public void DescribesItSelfAsInSpanMonths()
        {
            for (int d = 0; d < 15; d++)
            {
                var date = Create();
                date.Span = d;
                Assert.AreEqual(
                    String.Format("in {0} months", d),
                    date.Description.ToLower());
            }
        }

        [TestMethod]
        public void YieldsProperInTwoMonthsByDefault()
        {
            var dates = Enumerable.Range(1, 12)
                .Select(m => new DateTime(2012, m, 10));

            foreach (var d in dates)
            {
                var date = Create();
                var result = date.GetDate(d);
                Assert.AreEqual(d.AddMonths(2), result);
            }
        }

        [TestMethod]
        public void YieldsInXMonthsForSpansGtTwo()
        {
            var dates = Enumerable.Range(1, 12)
                .Select(m => new DateTime(2012, m, 10));

            for (int d = 3; d < 20; d++)
            {
                foreach (var baseDate in dates)
                {
                    var date = Create();
                    date.Span = d;
                    var result = date.GetDate(baseDate);

                    Assert.AreEqual(baseDate.AddMonths(d), result,
                        String.Format(
                        "For Span={0} and date={1:yyyy-MM-dd} got {2:yyyy-MM-dd}",
                        d, baseDate, result));
                }
            }
        }

        [TestMethod]
        public void YieldsDateInSpanMonthsEvenForTheEndOfMonth()
        {
            var ends = Enumerable.Range(2, 11)
                .Select(m => new DateTime(2012, m, 1).AddDays(-1)).ToList();
            ends.Add(new DateTime(2012, 12, 31));
            ends.Add(new DateTime(2013, 1, 31));

            for (int d = 0; d < 10; d++)
            {
                foreach (var eom in ends)
                {
                    var date = Create();
                    date.Span = d;
                    var result = date.GetDate(eom);
                    Assert.AreEqual(eom.AddMonths(d), result,
                        String.Format(
                        "For Span={0} and date={1:yyyy-MM-dd} got {2:yyyy-MM-dd}",
                        d, eom, result));
                }
            }
        }
    }
}
