﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestPlatform.UnitTestFramework;

using Appoint.Models;

namespace Appoint.Tests
{
    [TestClass]
    public class TodayDateTests : BaseDateTests
    {
        protected override BaseDate Create()
        {
            return new TodayDate();
        }

        protected override string Kind
        {
            get { return TodayDate.Kind; }
        }

        protected override DateTime GetDefault(DateTime date)
        {
            return date.Date;
        }

        [TestMethod]
        public void AlwaysYieldsTodayIndependantlyOfTheSpan()
        {
            for (int d = 0;  d < 100; d++)
            {
                var date = Create();
                date.Span = d;

                Assert.AreEqual(DateTime.Today, date.Date);
            }
        }

        public override void IsProperlyAssembledFromParams() { }
    }

    [TestClass]
    public class TomorrowDateTests : BaseDateTests
    {
        protected override BaseDate Create()
        {
            return new TomorrowDate();
        }

        protected override string Kind
        {
            get { return TomorrowDate.Kind; }
        }

        protected override DateTime GetDefault(DateTime date)
        {
            return date.AddDays(1);
        }

        [TestMethod]
        public void AlwaysYieldsTomorrowIndependantlyOfTheSpan()
        {
            for (int d = 0; d < 100; d++)
            {
                var date = Create();
                date.Span = d;

                Assert.AreEqual(DateTime.Today.AddDays(1), date.Date);
            }
        }

        public override void IsProperlyAssembledFromParams() { }
    }
}
