﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Phone.Tasks;
using Microsoft.VisualStudio.TestPlatform.UnitTestFramework;

using Appoint.Models;

namespace Appoint.Tests
{
    [TestClass]
    public class AppointmentTests
    {
        [TestMethod]
        public void DefaultsToTodayNinePM()
        {
            var appointment = new Appointment();

            Assert.AreEqual(DateTime.Today, appointment.Date.Date);
            Assert.AreEqual(new TimeSpan(21, 0, 0), appointment.Time.Span);
        }

        [TestMethod]
        public void YieldsParamsAsDateParamsAmpConcatenatedWithTimeParams()
        {
            var date = new InDaysDate { Span = 5 };
            var time = new Time { Hour = 22, Minute = 25 };

            var appointment = new Appointment
            {
                Date = date,
                Time = time
            };

            Assert.AreEqual(
                String.Format("{0}&{1}", date.Params, time.Params),
                appointment.Params);
        }

        [TestMethod]
        public void YieldsAppointmentTaskWithProperDateAndTime()
        {
            var date = new InDaysDate { Span = 5 };
            var time = new Time { Hour = 22, Minute = 25 };

            var appointment = new Appointment { Date = date, Time = time };
            var task = appointment.Task;

            Assert.AreEqual(date.Date, task.StartTime.Value.Date);
            Assert.AreEqual(time.Span, task.StartTime.Value.TimeOfDay);
        }
    }
}
