﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;
using Microsoft.VisualStudio.TestPlatform.UnitTestFramework;

using Appoint.Models;

namespace Appoint.Tests
{
    [TestClass]
    public class ThisMonthDateTests : BaseDateTests
    {
        protected override BaseDate Create()
        {
            return new ThisMonthDate();
        }

        protected override string Kind
        {
            get { return AppointmentFactory.Kinds.ThisMonth; }
        }

        protected override DateTime GetDefault(DateTime date)
        {
            var nextMonth = date.AddMonths(1);
            var firstDayOfNextMonth = new DateTime(nextMonth.Year, nextMonth.Month, 1);
            return firstDayOfNextMonth.AddDays(-1) + date.TimeOfDay;
        }

        [TestMethod]
        public void YieldsEndOfThisMonthByDefault()
        {
            var date = Create();
            Assert.AreEqual(DateTime.Today.Month, date.Date.Month);
            Assert.AreNotEqual(DateTime.Today.Month, date.Date.AddDays(1).Month);

            var daysDiffToToday = (date.Date - DateTime.Today).TotalDays;
            Assert.IsTrue(0 <= daysDiffToToday && daysDiffToToday <= 31);
        }

        [TestMethod]
        public void DescribesItSelfAsEndOfThisMonthByDefault()
        {
            var date = Create();
            Assert.AreEqual("end of month", date.Description.ToLower());
        }

        [TestMethod]
        public void DescribesItSelfAsOneDayBeforeTheEndOfThisMonthForSpanOne()
        {
            var date = Create();
            date.Span = 1;
            Assert.AreEqual("1 day before the end of month", date.Description.ToLower());
        }

        [TestMethod]
        public void DescribesItSelfAsSpanDaysBeforeTheEndOfThisMonthForPositiveSpan()
        {
            for (int d = 2; d < 15; d++)
            {
                var date = Create();
                date.Span = d;
                Assert.AreEqual(
                    String.Format("{0} days before the end of month", d),
                    date.Description.ToLower());
            }
        }

        [TestMethod]
        public void YieldsProperEndOfMonthByDefault()
        {
            var ends = Enumerable.Range(2,11)
                .Select(m => new DateTime(2012, m, 1).AddDays(-1)).ToList();
            ends.Add(new DateTime(2012,12,31));

            foreach (var d in ends)
            {
                var date = Create();
                var result = date.GetDate(d.AddDays(-15));
                Assert.AreEqual(d, result);
            }
        }

        [TestMethod]
        public void YieldsDateSpanDaysBeforeEndOfMonth()
        {
            var ends = Enumerable.Range(2, 11)
                .Select(m => new DateTime(2012, m, 1).AddDays(-1)).ToList();
            ends.Add(new DateTime(2012, 12, 31));

            for (int d = 0; d < 10; d++)
            {
                foreach (var eom in ends)
                {
                    var date = Create();
                    date.Span = d;
                    var result = date.GetDate(eom.AddDays(-15));
                    Assert.AreEqual(eom.AddDays(-d), result, 
                        String.Format(
                        "For Span={0} and date={1:yyyy-MM-dd} got {2:yyyy-MM-dd}",
                        d,eom.AddDays(-15),result));
                }
            }
        }



        [TestMethod]
        public void YieldsDateSpanDaysBeforeEndOfMonthEvenForTheEndOfMonth()
        {
            var ends = Enumerable.Range(2, 11)
                .Select(m => new DateTime(2012, m, 1).AddDays(-1)).ToList();
            ends.Add(new DateTime(2012, 12, 31));

            for (int d = 0; d < 10; d++)
            {
                foreach (var eom in ends)
                {
                    var date = Create();
                    date.Span = d;
                    var result = date.GetDate(eom);
                    Assert.AreEqual(eom.AddDays(-d), result,
                        String.Format(
                        "For Span={0} and date={1:yyyy-MM-dd} got {2:yyyy-MM-dd}",
                        d, eom, result));
                }
            }
        }
    }
}
