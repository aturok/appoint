﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestPlatform.UnitTestFramework;

using Appoint.Models;

namespace Appoint.Tests
{
    public abstract class BaseDateTests
    {
        protected abstract BaseDate Create();
        protected abstract string Kind { get; }

        protected abstract DateTime GetDefault(DateTime date);

        [TestMethod]
        public virtual void IsProperlyAssembledFromParams()
        {
            for (int d = 0; d <= 100; ++d)
            {
                var p = new Dictionary<string, string>();
                p["relspan"] = d.ToString();

                var date = Create().FromParams(p);

                Assert.AreEqual(d, date.Span, "Failed for " + Kind);
            }
        }

        [TestMethod]
        public void ThrowsKeyNotFoundOnSpanMissingInParams()
        {
            try
            {
                var p = new Dictionary<string, string>();
                var date = new InDaysDate().FromParams(p);
                Assert.Fail("Failed for " + Kind);
            }
            catch (KeyNotFoundException) { }
        }

        [TestMethod]
        public void ThrowsFormatOnCrapInSpanParameter()
        {
            var craps = new string[] { "crap", "123crap", "", "crap123" };

            foreach (var c in craps)
            {
                try
                {
                    var p = new Dictionary<string, string>();
                    p["relspan"] = c;

                    var date = new InDaysDate().FromParams(p);
                    Assert.Fail("Failed for " + Kind);
                }
                catch (FormatException) { }
            }
        }

        [TestMethod]
        public virtual void PutsSpanAndKindInParams()
        {
            for (int d = 0; d < 100; d++)
            {
                var date = Create();
                date.Span = d;

                string requiredParams = String.Format("kind={0}&relspan={1}", Kind, date.Span);
                Assert.IsTrue(date.Params.Contains(requiredParams), 
                    "Failed for " + Kind + " " + requiredParams + " didn't show up in " + date.Params);
            }
        }

        [TestMethod]
        public virtual void YieldsProperDefault()
        {
            var date = Create();

            Assert.AreEqual(GetDefault(DateTime.Today).Date, date.Date, "Failed for " + Kind);
            Assert.AreEqual(0, date.Date.TimeOfDay.TotalSeconds, "Failed for " + Kind);
        }
    }
}
