﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestPlatform.UnitTestFramework;

using Appoint.Models;

namespace Appoint.Tests
{
    [TestClass]
    public class InAWeekDateTests : BaseDateTests
    {
        protected override BaseDate Create()
        {
            return new InAWeekDate();
        }

        protected override string Kind
        {
            get { return InAWeekDate.Kind; }
        }

        protected override DateTime GetDefault(DateTime date)
        {
            return date.AddDays(7);
        }

        [TestMethod]
        public void AlwaysYieldsInAWeekIndependantlyOfTheSpan()
        {
            for (int d = 0; d < 100; d++)
            {
                var date = Create();
                date.Span = d;

                Assert.AreEqual(DateTime.Today.AddDays(7), date.Date);
            }
        }

        public override void IsProperlyAssembledFromParams() { }
    }
}
