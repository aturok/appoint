﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;
using Microsoft.VisualStudio.TestPlatform.UnitTestFramework;

using Appoint.Models;

namespace Appoint.Tests
{
    [TestClass]
    public class InAMonthDateTests : BaseDateTests
    {
        protected override BaseDate Create()
        {
            return new InAMonthDate();
        }

        protected override string Kind
        {
            get { return AppointmentFactory.Kinds.InAMonth; }
        }

        protected override DateTime GetDefault(DateTime date)
        {
            return date.AddMonths(1);
        }

        [TestMethod]
        public void DescribesItSelfAsInAMonth()
        {
            for (int d = 0; d < 15; d++)
            {
                var date = Create();
                date.Span = d;
                Assert.AreEqual("in a month", date.Description.ToLower());
            }
        }

        [TestMethod]
        public void YieldsProperInAMonthByDefault()
        {
            var dates = Enumerable.Range(1, 12)
                .Select(m => new DateTime(2012, m, 10));

            foreach (var d in dates)
            {
                var date = Create();
                var result = date.GetDate(d);
                Assert.AreEqual(d.AddMonths(1), result);
            }
        }

        [TestMethod]
        public void YieldsInAMonthForAllSpans()
        {
            var dates = Enumerable.Range(1, 12)
                .Select(m => new DateTime(2012, m, 10));

            for (int d = 0; d < 20; d++)
            {
                foreach (var baseDate in dates)
                {
                    var date = Create();
                    date.Span = d;
                    var result = date.GetDate(baseDate);

                    Assert.AreEqual(baseDate.AddMonths(1), result,
                        String.Format(
                        "For Span={0} and date={1:yyyy-MM-dd} got {2:yyyy-MM-dd}",
                        d, baseDate, result));
                }
            }
        }

        [TestMethod]
        public void YieldsDateInAMonthEvenForTheEndOfMonthForAllSpans()
        {
            var ends = Enumerable.Range(2, 11)
                .Select(m => new DateTime(2012, m, 1).AddDays(-1)).ToList();
            ends.Add(new DateTime(2012, 12, 31));
            ends.Add(new DateTime(2013, 1, 31));

            for (int d = 0; d < 10; d++)
            {
                foreach (var eom in ends)
                {
                    var date = Create();
                    date.Span = d;
                    var result = date.GetDate(eom);
                    Assert.AreEqual(eom.AddMonths(1), result,
                        String.Format(
                        "For Span={0} and date={1:yyyy-MM-dd} got {2:yyyy-MM-dd}",
                        d, eom, result));
                }
            }
        }

        [TestMethod]
        public void SpanWillAlwaysEqualOne()
        {
            for (int d = 0; d <= 100; ++d)
            {
                var date = Create();
                date.Span = d;

                Assert.AreEqual(1, date.Span);
            }
        }

        public override void IsProperlyAssembledFromParams() { }

        [TestMethod]
        public void SpanWillAlwaysEqualOneWhenAssembledFromParams()
        {
            for (int d = 0; d <= 100; ++d)
            {
                var p = new Dictionary<string, string>();
                p["relspan"] = d.ToString();

                var date = Create().FromParams(p);

                Assert.AreEqual(1, date.Span);
            }
        }
    }
}
