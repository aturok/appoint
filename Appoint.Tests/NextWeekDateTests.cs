﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;
using Microsoft.VisualStudio.TestPlatform.UnitTestFramework;

using Appoint.Models;

namespace Appoint.Tests
{
    [TestClass]
    public class NextWeekDateTests : BaseDateTests
    {
        protected override BaseDate Create()
        {
            return new NextWeekDate();
        }

        protected override string Kind
        {
            get { return AppointmentFactory.Kinds.NextWeek; }
        }

        protected override DateTime GetDefault(DateTime date)
        {
            var dayOfWeek = date.DayOfWeek;
            var spanToSaturday = DayOfWeek.Saturday - dayOfWeek;
            return date.AddDays(7 + spanToSaturday);
        }

        [TestMethod]
        public void YieldsProperDaysOfWeekAccordingToSpan()
        {
            foreach (var dof in Enum.GetValues(typeof(DayOfWeek)))
            {
                var date = Create();
                date.Span = (int)dof;

                Assert.AreEqual(dof, date.Date.DayOfWeek);
            }
        }

        [TestMethod]
        public void YieldsNextSaturdayByDefault()
        {
            var date = Create();
            Assert.AreEqual(DayOfWeek.Saturday, date.Date.DayOfWeek);

            var daysDiff = (DateTime.Today - date.Date).TotalDays;
            Assert.IsTrue(-13 <= daysDiff && daysDiff <= -7,
                String.Format("Days diff was: {0}, date was: {1}", daysDiff, date.Date.Date));
        }

        [TestMethod]
        public void DescribesItSelfAsNextDayOfWeek()
        {
            var sunday = default(DateTime).AddDays(6);
            for (int d = 0; d < 7; d++)
            {
                var date = Create();
                date.Span = d;

                string shouldBe = String.Format(new CultureInfo("en-US"), "next {0:dddd}", sunday.AddDays(d)).ToLower();
                Assert.AreEqual(shouldBe, date.Description.ToLower());
            }
        }
    }
}
