﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestPlatform.UnitTestFramework;

using Appoint.Models;

namespace Appoint.Tests
{
    [TestClass]
    public class RandomDateTests : BaseDateTests
    {
        protected const int _Seed = 2014;

        protected override BaseDate Create()
        {
            return new RandomDate(new Random(_Seed));
        }

        protected override string Kind
        {
            get { return AppointmentFactory.Kinds.Random; }
        }

        protected override DateTime GetDefault(DateTime date)
        {
            throw new NotImplementedException();
        }

        public override void YieldsProperDefault() { }

        [TestMethod]
        public void GeneratesDatesInSpanRange()
        {
            var r = new Random(_Seed);
            var dateRates = Enumerable.Range(0, 360).Select(i => r.NextDouble());

            int span = 360;

            var rDate = Create();
            rDate.Span = span;
            var baseDate = new DateTime(2013,7,12);
            foreach (var rate in dateRates)
            {
                var d = rDate.GetDate(baseDate);
                Assert.AreEqual(baseDate.AddDays((int)(span * rate)), d);
            }
        }

        [TestMethod]
        public void DefaultSpanIs100Days()
        {
            var d = Create();
            Assert.AreEqual(100, d.Span);
        }

        [TestMethod]
        public void DescribesItselfAsOnRandomDayInLessThanSpanDays()
        {
            foreach (var span in Enumerable.Range(2, 700))
            {
                var d = Create();
                d.Span = span;

                Assert.AreEqual(
                    String.Format("on random date in less than {0} days", span),
                    d.Description);
            }
        }

        [TestMethod]
        public void WillEventuallyGenerateSomeDatesCloseToSpanBoundForLargeSpan()
        {
            var d = Create();
            d.Span = 700;

            var baseDate = new DateTime(2014, 2, 23);

            foreach(var i in Enumerable.Range(0,10000))
            {
                var date = d.GetDate(baseDate);
                if (date > baseDate.AddDays(600))
                    return;
            }

            Assert.Fail("Did not generate sufficiently high date");
        }
    }
}
