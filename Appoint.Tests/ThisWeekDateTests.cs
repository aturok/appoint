﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;
using Microsoft.VisualStudio.TestPlatform.UnitTestFramework;

using Appoint.Models;

namespace Appoint.Tests
{
    [TestClass]
    public class ThisWeekDateTests : BaseDateTests
    {
        protected override BaseDate Create()
        {
            return new ThisWeekDate();
        }

        protected override string Kind
        {
            get { return AppointmentFactory.Kinds.ThisWeek; }
        }

        protected override DateTime GetDefault(DateTime date)
        {
            var dayOfWeek = date.DayOfWeek;
            var spanToSaturday = DayOfWeek.Saturday - dayOfWeek;
            return date.AddDays(spanToSaturday);
        }

        [TestMethod]
        public void YieldsProperDaysOfWeekAccordingToSpan()
        {
            foreach (var dof in Enum.GetValues(typeof(DayOfWeek)))
            {
                var date = Create();
                date.Span = (int)dof;

                Assert.AreEqual(dof, date.Date.DayOfWeek);
            }
        }

        [TestMethod]
        public void YieldsThisSaturdayByDefault()
        {
            var date = Create();
            Assert.AreEqual(DayOfWeek.Saturday, date.Date.DayOfWeek);

            var daysDiff = (DateTime.Today - date.Date).TotalDays;
            Assert.IsTrue(Math.Abs(daysDiff) < 7);
        }

        [TestMethod]
        public void DescribesItSelfAsThisDayOfWeek()
        {
            var sunday = default(DateTime).AddDays(6);
            for (int d = 0; d < 7; d++ )
            {
                var date = Create();
                date.Span = d;

                string shouldBe = String.Format(new CultureInfo("en-US"), "this {0:dddd}", sunday.AddDays(d)).ToLower();
                Assert.AreEqual(shouldBe, date.Description.ToLower());
            }
        }

        [TestMethod]
        public void PutsWeekEndsOnToParamsIntoParamsIfNotSaturday()
        {
            foreach (DayOfWeek day in Enum.GetValues(typeof(DayOfWeek)))
            {
                if (day != DayOfWeek.Saturday)
                {
                    var date = (ThisWeekDate)Create();
                    date.WeekEndsOn = (int)day;

                    var paramsStr = date.Params;
                    var p = AppointmentFactory.ParamsFromString(paramsStr);

                    Assert.IsTrue(p.ContainsKey(ThisWeekDate.WeekEndsOnKey));
                    Assert.AreEqual(((int)day).ToString(), p[ThisWeekDate.WeekEndsOnKey]);
                }
            }
        }

        [TestMethod]
        public void WeekEndsOnSaturdayByDefault()
        {
            var date = (ThisWeekDate)Create();
            Assert.AreEqual((int)DayOfWeek.Saturday, date.WeekEndsOn);
        }

        [TestMethod]
        public void CollectsWeekEndsOnFromParams()
        {

            foreach (DayOfWeek day in Enum.GetValues(typeof(DayOfWeek)))
            {
                var p = new Dictionary<string, string>();
                p["kind"] = ThisWeekDate.Kind;
                p["relspan"] = ((int)DayOfWeek.Saturday).ToString();
                p[ThisWeekDate.WeekEndsOnKey] = ((int)day).ToString();

                var date = (ThisWeekDate)Create().FromParams(p);

                Assert.AreEqual((int)day, date.WeekEndsOn);
            }
        }

        [TestMethod]
        public void AssumesSaturdayIfWeekEndsOnOmittedFromParams()
        {
            var p = new Dictionary<string, string>();
            p["kind"] = ThisWeekDate.Kind;
            p["relspan"] = ((int)DayOfWeek.Saturday).ToString();

            var date = (ThisWeekDate)Create().FromParams(p);

            Assert.AreEqual((int)DayOfWeek.Saturday, date.WeekEndsOn);
        }

        [TestMethod]
        public virtual void CorrectlyLandsThisWeekIfWeekEndsOnSunday()
        {
            var date = (ThisWeekDate)Create();
            date.WeekEndsOn = (int)DayOfWeek.Sunday;

            var monday = new DateTime(2014, 2, 10);
            var weekDays = Enumerable.Range(0, 7).Select(i => monday.AddDays(i));

            foreach (var desiredDay in weekDays)
            {
                date.Span = (int)desiredDay.DayOfWeek;
                foreach (var d in weekDays)
                {
                    Assert.AreEqual(desiredDay.Date, date.GetDate(d));
                }
            }
        }

        [TestMethod]
        public virtual void CorrectlyLandsThisWeekIfWeekEndsOnSaturday()
        {
            var date = (ThisWeekDate)Create();
            date.WeekEndsOn = (int)DayOfWeek.Saturday;

            var monday = new DateTime(2014, 2, 10);
            var weekDays = Enumerable.Range(-1, 7).Select(i => monday.AddDays(i));

            foreach (var desiredDay in weekDays)
            {
                date.Span = (int)desiredDay.DayOfWeek;
                foreach (var d in weekDays)
                {
                    Assert.AreEqual(desiredDay.Date, date.GetDate(d));
                }
            }
        }
    }
}
