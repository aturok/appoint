﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;
using Microsoft.VisualStudio.TestPlatform.UnitTestFramework;

using Appoint.Models;

namespace Appoint.Tests
{
    [TestClass]
    public class NextMonthDateTests : BaseDateTests
    {
        protected override BaseDate Create()
        {
            return new NextMonthDate();
        }

        protected override string Kind
        {
            get { return AppointmentFactory.Kinds.NextMonth; }
        }

        protected override DateTime GetDefault(DateTime date)
        {
            var nextNextMonth = date.AddMonths(2);
            var firstDayOfNextNextMonth = new DateTime(nextNextMonth.Year, nextNextMonth.Month, 1);
            return firstDayOfNextNextMonth.AddDays(-1) + date.TimeOfDay;
        }

        [TestMethod]
        public void YieldsEndOfNextMonthByDefault()
        {
            var date = Create();
            Assert.AreEqual(DateTime.Today.AddMonths(1).Month, date.Date.Month);
            Assert.AreNotEqual(DateTime.Today.AddMonths(1).Month, date.Date.AddDays(1).Month);

            var daysDiffToToday = (date.Date - DateTime.Today).TotalDays;
            Assert.IsTrue(28 <= daysDiffToToday && daysDiffToToday <= 62);
        }

        [TestMethod]
        public void DescribesItSelfAsEndOfThisMonthByDefault()
        {
            var date = Create();
            Assert.AreEqual("end of next month", date.Description.ToLower());
        }

        [TestMethod]
        public void DescribesItSelfAsOneDayBeforeTheEndOfThisMonthForSpanOne()
        {
            var date = Create();
            date.Span = 1;
            Assert.AreEqual("1 day before the end of next month", date.Description.ToLower());
        }

        [TestMethod]
        public void DescribesItSelfAsSpanDaysBeforeTheEndOfThisMonthForPositiveSpan()
        {
            for (int d = 2; d < 15; d++)
            {
                var date = Create();
                date.Span = d;
                Assert.AreEqual(
                    String.Format("{0} days before the end of next month", d),
                    date.Description.ToLower());
            }
        }

        [TestMethod]
        public void YieldsProperEndOfNextMonthByDefault()
        {
            var ends = Enumerable.Range(2, 11)
                .Select(m => new DateTime(2012, m, 1).AddDays(-1)).ToList();
            ends.Add(new DateTime(2012, 12, 31));

            foreach (var d in ends)
            {
                var date = Create();
                var result = date.GetDate(d.AddDays(-45));
                Assert.AreEqual(d, result);
            }
        }

        [TestMethod]
        public void YieldsDateSpanDaysBeforeEndOfNextMonth()
        {
            var ends = Enumerable.Range(2, 11)
                .Select(m => new DateTime(2012, m, 1).AddDays(-1)).ToList();
            ends.Add(new DateTime(2012, 12, 31));

            for (int d = 0; d < 10; d++)
            {
                foreach (var eom in ends)
                {
                    var date = Create();
                    date.Span = d;
                    var result = date.GetDate(eom.AddDays(-45));
                    Assert.AreEqual(eom.AddDays(-d), result,
                        String.Format(
                        "For Span={0} and date={1:yyyy-MM-dd} got {2:yyyy-MM-dd}",
                        d, eom.AddDays(-15), result));
                }
            }
        }



        [TestMethod]
        public void YieldsDateSpanDaysBeforeEndOfNextMonthEvenForTheEndOfMonth()
        {
            var ends = Enumerable.Range(2, 11)
                .Select(m => new DateTime(2012, m, 1).AddDays(-1)).ToList();
            ends.Add(new DateTime(2012, 12, 31));
            ends.Add(new DateTime(2013, 1, 31));

            for (int d = 0; d < 10; d++)
            {
                for (int i = 0; i < 12; i++)
                {
                    var eom = ends[i];
                    var date = Create();
                    date.Span = d;
                    var result = date.GetDate(eom);
                    Assert.AreEqual(ends[i + 1].AddDays(-d), result,
                        String.Format(
                        "For Span={0} and date={1:yyyy-MM-dd} got {2:yyyy-MM-dd}",
                        d, eom, result));
                }
            }
        }
    }
}
