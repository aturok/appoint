﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestPlatform.UnitTestFramework;

using Appoint.Models;

namespace Appoint.Tests
{
    [TestClass]
    public class InWeeksDateTests : BaseDateTests
    {
        protected override BaseDate Create()
        {
            return new InWeeksDate();
        }

        protected override string Kind
        {
            get { return InWeeksDate.Kind; }
        }

        protected override DateTime GetDefault(DateTime date)
        {
            return DateTime.Today.AddDays(14);
        }

        [TestMethod]
        public void YieldsProperDateBasedOnTodayWithoutTime()
        {
            for (int w = 0; w <= 100; ++w)
            {
                var date = Create();
                date.Span = w;

                Assert.AreEqual(DateTime.Today.Date.AddDays(w*7), date.Date);
                Assert.AreEqual(0, date.Date.TimeOfDay.TotalSeconds);
            }
        }
    }
}
