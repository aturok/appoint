﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestPlatform.UnitTestFramework;

using Appoint.Models;

namespace Appoint.Tests
{
    [TestClass]
    public class DurationTests
    {
        [TestMethod]
        public void Is60MinutesByDefault()
        {
            var d = new Duration();
            Assert.AreEqual(60, d.TotalMinutes);
        }

        [TestMethod]
        public void ShowsTotalMinutes()
        {
            foreach (var i in Enumerable.Range(-1, 1000))
            {
                var d = new Duration { TotalMinutes = i };
                Assert.AreEqual(i, d.TotalMinutes);
            }
        }

        [TestMethod]
        public void GivesTotalMinutesInParametersDict()
        {
            foreach (var i in Enumerable.Range(-1, 1000))
            {
                var d = new Duration { TotalMinutes = i };
                Assert.IsTrue(d.Params.Contains(String.Format("duration={0}", i)));
            }
        }

        [TestMethod]
        public void RestoresTotalMinutesFromParametersString()
        {
            foreach (var i in Enumerable.Range(-1, 1000))
            {
                var pars = new Dictionary<string, string>();
                pars["duration"] = i.ToString();
                var d = new Duration().FromParams(pars);
                Assert.AreEqual(i, d.TotalMinutes);
            }
        }

        [TestMethod]
        public void Assumes1hIfDurationMissingFromParams()
        {
            var fromEmpty = new Duration().FromParams(new Dictionary<string, string>());
            Assert.AreEqual(60, fromEmpty.TotalMinutes);

            var crapParams = new Dictionary<string, string>();
            crapParams["h"] = "20";
            crapParams["m"] = "50";
            crapParams["kind"] = "stuff";
            var fromCrap = new Duration().FromParams(crapParams);
            Assert.AreEqual(60, fromCrap.TotalMinutes);
        }

        [TestMethod]
        public void ReturnsProperEndDateForSimpleMinutesDuration()
        {
            var start = new DateTime(2014, 3, 8, 16, 0, 0);
            foreach (var i in Enumerable.Range(0, 1000))
            {
                var d = new Duration { TotalMinutes = i };
                var end = d.CreateEnd(start);

                var span = end - start;

                Assert.AreEqual(i, span.TotalMinutes);
            }
        }

        [TestMethod]
        public void ReturnsEndOfTheDayForNegative1Duration()
        {
            foreach (var h in Enumerable.Range(0, 24))
            {
                foreach (var m in Enumerable.Range(0, 60))
                {
                    var start = new DateTime(2014, 8, 3, h, m, 0);
                    var d = new Duration { TotalMinutes = -1 };
                    var end = d.CreateEnd(start);

                    Assert.AreEqual(start.Date, end.Date);
                    Assert.AreEqual(23, end.Hour);
                    Assert.AreEqual(59, end.Minute);
                }
            }
        }
    }
}
