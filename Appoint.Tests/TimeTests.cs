﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestPlatform.UnitTestFramework;

using Appoint.Models;

namespace Appoint.Tests
{
    [TestClass]
    public class TimeTests
    {
        [TestMethod]
        public void ParsesAnyHoursAndMinutesFromZeroToHundredFromParams()
        {
            for (int h = 0; h <= 100; h++)
            {
                for (int m = 0; m <= 100; m++)
                {
                    var d = new Dictionary<string, string>();
                    d["h"] = h.ToString();
                    d["m"] = m.ToString();

                    var time = new Time().FromParams(d);
                    Assert.AreEqual(h, time.Hour);
                    Assert.AreEqual(m, time.Minute);
                }
            }
        }

        [TestMethod]
        public void AssumesHourIsZeroIfMissing()
        {
            for (int m = 0; m <= 100; m++)
            {
                var d = new Dictionary<string, string>();
                d["m"] = m.ToString();

                var time = new Time().FromParams(d);
                Assert.AreEqual(0, time.Hour);
                Assert.AreEqual(m, time.Minute);
            }
        }

        [TestMethod]
        public void AssumesMinuteIsZeroIfMissing()
        {
            for (int h = 0; h <= 100; h++)
            {
                var d = new Dictionary<string, string>();
                d["h"] = h.ToString();

                var time = new Time().FromParams(d);
                Assert.AreEqual(h, time.Hour);
                Assert.AreEqual(0, time.Minute);
            }
        }

        [TestMethod]
        public void AssumesEverythingIsZeroIfHoursAndMinutesMissing()
        {
            var time = new Time().FromParams(new Dictionary<string, string>());

            Assert.AreEqual(0, time.Hour);
            Assert.AreEqual(0, time.Minute);
        }

        [TestMethod]
        public void ThrowsFormatExceptionIfCrapIsSpecifiedForHours()
        {
            var crap = new Dictionary<string, string>();
            crap["h"] = "crap";

            try
            {
                var time = new Time().FromParams(crap);
                Assert.Fail("Did not throw on simple crap");
            }
            catch (FormatException) { }

            var anotherCrap = new Dictionary<string, string>();
            anotherCrap["h"] = "123crap";

            try
            {
                var time = new Time().FromParams(crap);
                Assert.Fail("Did not throw on simple crap");
            }
            catch (FormatException) { }
        }

        [TestMethod]
        public void ThrowsFormatExceptionIfCrapIsSpecifiedForMinutes()
        {
            var crap = new Dictionary<string, string>();
            crap["m"] = "crap";

            try
            {
                var time = new Time().FromParams(crap);
                Assert.Fail("Did not throw on simple crap");
            }
            catch (FormatException) { }

            var anotherCrap = new Dictionary<string, string>();
            anotherCrap["m"] = "123crap";

            try
            {
                var time = new Time().FromParams(crap);
                Assert.Fail("Did not throw on crap beginning with digits");
            }
            catch (FormatException) { }
        }

        [TestMethod]
        public void OutputsProperFormatString()
        {
            for (int h = 0; h <= 100; h++)
            {
                for (int m = 0; m <= 100; m++)
                {
                    var time = new Time { Hour = h, Minute = m };

                    Assert.AreEqual(
                        String.Format("h={0}&m={1}", h, m),
                        time.Params);
                }
            }
        }

        [TestMethod]
        public void OutputsProperTimespanWithoutSecondsAndMilliForTimeOfTheDayHoursAndMinutes()
        {
            for (int h = 0; h <= 23; h++)
            {
                for (int m = 0; m <= 59; m++)
                {
                    var time = new Time { Hour = h, Minute = m };
                    var span = time.Span;

                    Assert.AreEqual(h, span.Hours);
                    Assert.AreEqual(m, span.Minutes);
                    Assert.AreEqual(0, span.Seconds);
                    Assert.AreEqual(0, span.Milliseconds);
                }
            }
        }

        [TestMethod]
        public void OutputsProperTimespanForHoursOnly()
        {
            for (int h = 0; h <= 100; h++)
            {
                var time = new Time { Hour = h };

                Assert.AreEqual(h, time.Span.TotalHours);

                Assert.AreEqual(0, time.Span.Minutes);
                Assert.AreEqual(0, time.Span.Seconds);
                Assert.AreEqual(0, time.Span.Milliseconds);
            }
        }

        [TestMethod]
        public void OutputsProperTimespanForMinutesOnly()
        {
            for (int m = 0; m <= 1000; m++)
            {
                var time = new Time { Minute = m };

                Assert.AreEqual(m, time.Span.TotalMinutes);

                Assert.AreEqual(0, time.Span.Seconds);
                Assert.AreEqual(0, time.Span.Milliseconds);
            }
        }

        [TestMethod]
        public void OutputsProperTimespanForWeirdHoursAndMinutes()
        {
            for (int h = 0; h <= 100; h++)
            {
                for (int m = 0; m <= 100; m++)
                {
                    var time = new Time { Hour = h, Minute = m };

                    Assert.AreEqual(h * 60 + m, time.Span.TotalMinutes);

                    Assert.AreEqual(0, time.Span.Seconds);
                    Assert.AreEqual(0, time.Span.Milliseconds);
                }
            }
        }

        [TestMethod]
        public void ProperlyReadsFromDateTime()
        {
            var todayZero = DateTime.Today.Date;

            for (int h = 0; h <= 23; h++)
            {
                for (int m = 0; m <= 59; m++)
                {
                    var timeOfDay = todayZero.AddHours(h).AddMinutes(m);
                    var time = new Time().FromTimeOfDay(timeOfDay);

                    Assert.AreEqual(h, time.Hour);
                    Assert.AreEqual(m, time.Minute);
                }
            }
        }

        [TestMethod]
        public void CurrentTimeCanBeOverwrittenFromDateTime()
        {
            int h = 21;
            int m = 15;
            var someTime = DateTime.Today.AddHours(h).AddMinutes(m);

            var time = new Time { Hour = 11, Minute = 35 };
            time.FromTimeOfDay(someTime);

            Assert.AreEqual(h, time.Hour);
            Assert.AreEqual(m, time.Minute);
        }

        [TestMethod]
        public void OutputsProperTimeOfDayWithDefaultDate()
        {
            for (int h = 0; h <= 23; h++)
            {
                for (int m = 0; m <= 59; m++)
                {
                    var time = new Time { Hour = h, Minute = m };
                    var timeOfDay = time.ToTimeOfDay();

                    Assert.AreEqual(h, timeOfDay.Hour);
                    Assert.AreEqual(m, timeOfDay.Minute);
                    Assert.AreEqual<DateTime>(default(DateTime), timeOfDay.Date);
                }
            }
        }

        [TestMethod]
        public void DescriptionIsHourColonMinuteZeroPadded()
        {
            for (int h = 0; h <= 23; h++)
            {
                for (int m = 0; m <= 59; m++)
                {
                    var time = new Time { Hour = h, Minute = m };
                    Assert.AreEqual(
                        String.Format("{0}:{1:D2}", h, m),
                        time.Description);
                }
            }
        }
    }
}
