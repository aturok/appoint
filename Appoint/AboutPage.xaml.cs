﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Microsoft.Phone.Tasks;

using Appoint.Resources;

namespace Appoint
{
    public partial class AboutPage : PhoneApplicationPage
    {
        public AboutPage()
        {
            InitializeComponent();
            ApplicationBar = CreateAppBar();
        }

        private void WwwButton_Click(object sender, EventArgs e)
        {
            var browserTask = new WebBrowserTask
            {
                Uri = new Uri(AppResources.WebsiteLink, UriKind.Absolute)
            };
            browserTask.Show();
        }

        private void EmailButton_Click(object sender, EventArgs e)
        {
            var mailTask = new EmailComposeTask
            {
                To = AppResources.SuggestionsEmailAddress,
                Subject = AppResources.SuggestionsEmailSubject
            };
            mailTask.Show();
        }

        private void RateButton_Click(object sender, EventArgs e)
        {
            var rateTask = new MarketplaceReviewTask();
            rateTask.Show();
        }

        public ApplicationBar CreateAppBar()
        {
            var bar = new ApplicationBar();

            var wwwButton = new ApplicationBarIconButton(new Uri("/Assets/Icons/appbar.ie.png", UriKind.Relative));
            wwwButton.Text = AppResources.AboutBarWwwButtonText;
            wwwButton.Click += WwwButton_Click;
            bar.Buttons.Add(wwwButton);

            var emailButton = new ApplicationBarIconButton(new Uri("/Assets/Icons/appbar.email.minimal.png", UriKind.Relative));
            emailButton.Text = AppResources.AboutBarEmailButtonText;
            emailButton.Click += EmailButton_Click;
            bar.Buttons.Add(emailButton);

            var rateButton = new ApplicationBarIconButton(new Uri("/Assets/Icons/appbar.marketplace.png", UriKind.Relative));
            rateButton.Text = AppResources.AboutBarRateButtonText;
            rateButton.Click += RateButton_Click;
            bar.Buttons.Add(rateButton);

            return bar;
        }
    }
}