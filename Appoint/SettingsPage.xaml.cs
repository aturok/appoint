﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

using Appoint.Models;

namespace Appoint
{
    public partial class SettingsPage : PhoneApplicationPage
    {
        private SettingsViewModel _vm;

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            _vm = new SettingsViewModel();
            DataContext = _vm;
            base.OnNavigatedTo(e);
        }

        public SettingsPage()
        {
            ApplicationBar = CreateAppBar();
            InitializeComponent();
        }

        private void RestoreDefaults_Click(object sender, RoutedEventArgs e)
        {
            var answer = MessageBox.Show("Are you sure you want to restore default shortcuts - you will lose your customizations?", "", MessageBoxButton.OKCancel);
            if (answer == MessageBoxResult.OK)
            {
                App.Context.ResetToDefaultShortcuts();
                NavigationService.GoBack();
            }
        }

        private ApplicationBar CreateAppBar()
        {
            var bar = new ApplicationBar();

            var saveItem = new ApplicationBarIconButton
            {
                Text = "save",
                IconUri = new Uri("/Assets/Icons/appbar.save.png", UriKind.Relative)
            };
            saveItem.Click += SaveItem_Click;
            bar.Buttons.Add(saveItem);

            return bar;
        }

        private void SaveItem_Click(object sender, EventArgs e)
        {
            _vm.Save();
            NavigationService.GoBack();
        }
    }
}