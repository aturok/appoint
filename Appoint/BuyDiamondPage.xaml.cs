﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

using Appoint.Models;

namespace Appoint
{
    public partial class BuyDiamondPage : PhoneApplicationPage
    {
        public BuyDiamondPage()
        {
            InitializeComponent();
        }

        private void buyButton_Click(object sender, RoutedEventArgs e)
        {
            Products.BuyDiamondAsync();
        }
    }
}