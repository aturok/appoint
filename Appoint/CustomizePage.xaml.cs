﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

using Appoint.Models;

namespace Appoint
{
    public partial class CustomizePage : PhoneApplicationPage
    {
        private AppointmentShortcut _vm;

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            var kind = NavigationContext.QueryString["kind"];
            _vm = App.Context.GetShortcut(kind);
            DataContext = _vm;

            base.OnNavigatedTo(e);
        }

        private void PinButton_Click(object sender, EventArgs e)
        {
            App.Context.Pin(_vm);
        }

        private void AppointButton_Click(object sender, EventArgs e)
        {
            _vm.Appointment.Task.Show();
        }

        private void SaveButton_Click(object sender, EventArgs e)
        {
            App.Context.Save();
            NavigationService.GoBack();
        }

        protected override void OnBackKeyPress(System.ComponentModel.CancelEventArgs e)
        {
            App.Context.Load();
            base.OnBackKeyPress(e);
        }

        public CustomizePage()
        {
            ApplicationBar = CreateAppBar();
            InitializeComponent();
        }

        private ApplicationBar CreateAppBar()
        {
            var appbar = new ApplicationBar { };

            var saveButon = new ApplicationBarIconButton
            {
                IconUri = new Uri("/Assets/Icons/appbar.save.png", UriKind.Relative),
                Text = "save"
            };
            saveButon.Click += SaveButton_Click;
            appbar.Buttons.Add(saveButon);

            var pinButton = new ApplicationBarIconButton
            {
                IconUri = new Uri("/Assets/Icons/appbar.pin.png", UriKind.Relative),
                Text = "pin"
            };
            pinButton.Click += PinButton_Click;
            appbar.Buttons.Add(pinButton);

            var appointButton = new ApplicationBarIconButton
            {
                IconUri = new Uri("/Assets/Icons/appbar.calendar.day.png", UriKind.Relative),
                Text = "appoint"
            };
            appointButton.Click += AppointButton_Click;
            appbar.Buttons.Add(appointButton);

            return appbar;
        }
    }
}