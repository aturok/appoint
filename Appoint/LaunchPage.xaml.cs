﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

using Appoint.Models;

namespace Appoint
{
    public partial class LaunchPage : PhoneApplicationPage
    {
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            if (NavigationContext.QueryString.Any() == false)
                App.Current.Terminate();

            var appointment = AppointmentFactory.Create(NavigationContext.QueryString);

            NavigationContext.QueryString.Clear();

            appointment.Appointment.Task.Show();
        }

        public LaunchPage()
        {
            InitializeComponent();
        }
    }
}