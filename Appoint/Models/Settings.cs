﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO.IsolatedStorage;

namespace Appoint.Models
{
    public static class Settings
    {
        private const string _WeekEndsOn = "WeekEndsOn";
        private const string _DontAskForRating = "DontAskForRating";
        private const string _LaunchCount = "LaunchCount";
        private const string _TapToCustomize = "TapToCustomize";
        private const string _DiamondAcquired = "DiamondAcquired";

        private static IsolatedStorageSettings AppSettings { get { return IsolatedStorageSettings.ApplicationSettings; } }

        public static int WeekEndsOn
        {
            get
            {
                if (AppSettings.Contains(_WeekEndsOn))
                    return (int)AppSettings[_WeekEndsOn];
                else
                    return (int)DayOfWeek.Sunday;
            }

            set
            {
                AppSettings[_WeekEndsOn] = value;
                AppSettings.Save();
            }
        }

        public static bool DontAskForRating
        {
            get
            {
                if (AppSettings.Contains(_DontAskForRating) == false)
                {
                    DontAskForRating = false;
                }
                return (bool)AppSettings[_DontAskForRating];
            }

            set
            {
                AppSettings[_DontAskForRating] = value;
                AppSettings.Save();
            }
        }

        public static bool TapToCustomize
        {
            get
            {
                if (AppSettings.Contains(_TapToCustomize) == false)
                {
                    TapToCustomize = false;
                }
                return (bool)AppSettings[_TapToCustomize];
            }

            set
            {
                AppSettings[_TapToCustomize] = value;
                AppSettings.Save();
            }
        }

        public static int LaunchCount
        {
            get
            {
                if (AppSettings.Contains(_LaunchCount) == false)
                {
                    LaunchCount = 0;
                }
                return (int)AppSettings[_LaunchCount];
            }

            private set
            {
                AppSettings[_LaunchCount] = value;
                AppSettings.Save();
            }
        }

        public static void IncLaunchCount()
        {
            LaunchCount = LaunchCount + 1;
        }

        public static bool IsTimeToAskForRating
        {
            get
            {
                return DontAskForRating != true && (LaunchCount % 20 == 0);
            }
        }

        public static bool DiamondAcquired
        {
            get
            {
                if (AppSettings.Contains(_DiamondAcquired) == false)
                {
                    DiamondAcquired = false;
                }
                return (bool)AppSettings[_DiamondAcquired];
            }
            set
            {
                AppSettings[_DiamondAcquired] = value;
            }
        }
    }
}
