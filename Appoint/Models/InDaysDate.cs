﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Appoint.Models
{
    public class InDaysDate : BaseDate
    {
        public const string Kind = "indays";

        public override string Description { get { return String.Format("in {0} days", Span); } }

        public override DateTime GetDate(DateTime baseDate)
        {
            return baseDate.AddDays(Span);
        }

        public InDaysDate()
            : this(Kind)
        {
        }

        protected InDaysDate(string kind)
            : base(kind)
        {
            Span = 10;
        }
    }

    public class TodayDate : InDaysDate
    {
        public new const string Kind = "today";

        public override int Span { get { return 0; } set { } }

        public override string Description { get { return "today"; } }

        public TodayDate()
            : base(Kind)
        {
        }
    }

    public class TomorrowDate : InDaysDate
    {
        public new const string Kind = "tomorrow";

        public override int Span { get { return 1; } set { } }

        public override string Description { get { return "tomorrow"; } }

        public TomorrowDate()
            : base(Kind)
        {
        }
    }
}
