﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace Appoint.Models
{
    public class SpanOption
    {
        public string Title { get; set; }
        public int Value { get; set; }
    }

    public class AppointmentShortcut : INotifyPropertyChanged
    {
        public string Title { get; set; }
        public string Description { get { return Appointment.Description; } }
        public string TileImageUri { get; set; }
        public string Kind { get; set; }
        public Appointment Appointment { get; set; }

        public DateTime AppointmentTime
        {
            get
            {
                return Appointment.Time.ToTimeOfDay();
            }
            set
            {
                Appointment.Time = Appointment.Time.FromTimeOfDay(value);
                TimeChanged();
            }
        }

        public bool IsSpanCustomizable { get { return SpanOptions != null && SpanOptions.Any(); } }
        public string SpanMessage { get; set; }

        public IEnumerable<SpanOption> SpanOptions { get; set; }

        private SpanOption _selectedSpan;
        public SpanOption SelectedSpan
        {
            get
            {
                if (_selectedSpan == null && SpanOptions != null && SpanOptions.Any())
                {
                    _selectedSpan = SpanOptions.FirstOrDefault(o => o.Value == Appointment.Date.Span);
                }
                return _selectedSpan;
            }
            set
            {
                ChangeSpan(value);
                SpanChanged();
            }
        }

        protected void ChangeSpan(SpanOption span)
        {
            _selectedSpan = span;
            if (_selectedSpan != null)
            {
                Appointment.Date.Span = _selectedSpan.Value;
            }
        }

        public IEnumerable<SpanOption> DurationOptions { get; set; }

        private SpanOption _selectedDuration;
        public SpanOption SelectedDuration
        {
            get
            {
                if (_selectedDuration == null)
                {
                    _selectedDuration = DurationOptions.FirstOrDefault(o => o.Value == Appointment.Duration.TotalMinutes);
                }
                return _selectedDuration;
            }
            set
            {
                _selectedDuration = value;
                Appointment.Duration.TotalMinutes = _selectedDuration.Value;
            }
        }

        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;

        private void TimeChanged()
        {
            OnPropertiesChanged("AppointmentTime", "Description");
        }

        private void SpanChanged()
        {
            OnPropertiesChanged("SelectedSpan", "Description");
        }

        protected void OnPropertiesChanged(params string[] properties)
        {
            if (PropertyChanged != null)
            {
                foreach (var p in properties)                {
                    PropertyChanged.Invoke(this, new PropertyChangedEventArgs(p));
                }
            }
        }

        #endregion INotifyPropertyChanged
    }
}
