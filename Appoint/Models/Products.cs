﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Windows.ApplicationModel.Store;

namespace Appoint.Models
{
    public class Products
    {
        private const string _DiamondKey = "diamondshortcut";

        public static bool IsDiamondAcquired
        {
            get
            {
                if (Settings.DiamondAcquired == false)
                {
                    CheckDiamondAcquired();
                }

                return Settings.DiamondAcquired;
            }
        }

        private static void CheckDiamondAcquired()
        {
            if (CurrentApp.LicenseInformation.ProductLicenses.ContainsKey(_DiamondKey) == false)
            {
                Settings.DiamondAcquired = false;
                return;
            }

            Settings.DiamondAcquired = CurrentApp.LicenseInformation.ProductLicenses[_DiamondKey].IsActive;
        }

        public static async void BuyDiamondAsync()
        {
            await CurrentApp.RequestProductPurchaseAsync(_DiamondKey, true);
        }

    }
}
