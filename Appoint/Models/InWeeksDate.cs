﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Appoint.Models
{
    public class InWeeksDate : BaseDate
    {
        public const string Kind = "inweeks";
        
        public override DateTime GetDate(DateTime baseDate)
        {
            return baseDate.AddDays(7 * Span);
        }

        public override string Description
        {
            get { return String.Format("in {0} weeks", Span); }
        }

        public InWeeksDate()
            : this(Kind)
        {
        }

        protected InWeeksDate(string kind)
            : base(kind)
        {
            Span = 2;
        }
    }

    public class InAWeekDate : InWeeksDate
    {
        public new const string Kind = "inaweek";

        public override int Span { get { return 1; } set { } }

        public override string Description { get { return "in a week"; } }

        public InAWeekDate()
            : base(Kind)
        {
        }
    }
}
