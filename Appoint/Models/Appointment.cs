﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Phone.Tasks;

namespace Appoint.Models
{
    public class Appointment
    {
        public BaseDate Date { get; set; }
        public Time Time { get; set; }
        public Duration Duration { get; set; }

        public string Params { get { return String.Format("{0}&{1}&{2}", Date.Params, Time.Params, Duration.Params); } }

        public string Description { get { return String.Format("{0} at {1}", Date.Description, Time.Description); } }

        public SaveAppointmentTask Task
        {
            get
            {
                var start = Date.Date + Time.Span;
                var end = Duration.CreateEnd(start);

                return new SaveAppointmentTask
                {
                    StartTime = start,
                    EndTime = end
                };
            }
        }

        public Appointment()
        {
            Date = new TodayDate { };
            Time = new Time { Hour = 21 };
            Duration = new Duration { };
        }
    }
}
