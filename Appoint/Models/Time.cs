﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Appoint.Models
{
    public class Time
    {
        public int Hour { get; set; }
        public int Minute { get; set; }

        public virtual TimeSpan Span { get { return new TimeSpan(Hour, Minute, 0); } }

        public string Params { get { return String.Format("h={0}&m={1}", Hour, Minute); } }

        public string Description { get { return String.Format("{0}:{1:D2}", Hour, Minute); } }

        public virtual Time FromParams(IDictionary<string, string> p)
        {
            string hour = null;
            string minute = null;

            p.TryGetValue("h", out hour);
            p.TryGetValue("m", out minute);

            hour = hour ?? "0";
            minute = minute ?? "0";

            Hour = Int32.Parse(hour);
            Minute = Int32.Parse(minute);
            return this;
        }

        public virtual Time FromTimeOfDay(DateTime time)
        {
            Hour = time.Hour;
            Minute = time.Minute;
            return this;
        }

        public virtual DateTime ToTimeOfDay()
        {
            return new DateTime().AddHours(Hour).AddMinutes(Minute);
        }
    }
}
