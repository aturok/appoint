﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Appoint.Models
{
    public class InMonthsDate : BaseDate
    {
        public const string Kind = "inmonths";

        public override string Description
        {
            get { return String.Format("in {0} months", Span); }
        }

        public override DateTime GetDate(DateTime baseDate)
        {
            return baseDate.AddMonths(Span);
        }

        public InMonthsDate()
            : this(Kind)
        {
        }

        protected InMonthsDate(string kind)
            : base(kind)
        {
            Span = 2;
        }
    }

    public class InAMonthDate : InMonthsDate
    {
        public new const string Kind = "inamonth";

        public override int Span { get { return 1; } set { } }

        public override string Description { get { return "in a month"; } }

        public InAMonthDate()
            : base(Kind)
        {
        }
    }
}
