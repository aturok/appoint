﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Appoint.Models
{
    public class Duration
    {
        private const string _DurationKey = "duration";

        public int TotalMinutes { get; set; }

        public string Params
        { 
            get 
            {
                return String.Format("{0}={1}", _DurationKey, TotalMinutes);
            }
        }

        public Duration FromParams(IDictionary<string,string> parameters)
        {
            if (parameters.ContainsKey(_DurationKey))
            {
                TotalMinutes = Int32.Parse(parameters[_DurationKey]);
            }
            return this;
        }

        public DateTime CreateEnd(DateTime start)
        {
            if (TotalMinutes >= 0)
                return start.AddMinutes(TotalMinutes);
            else
            {
                return new DateTime(start.Year, start.Month, start.Day, 23, 59, 0);
            }
        }

        public Duration()
        {
            TotalMinutes = 60;
        }
    }
}
