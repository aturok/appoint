﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO.IsolatedStorage;
using System.Xml.Serialization;
using Microsoft.Phone.Shell;
using System.Windows;

using Appoint.Models;

namespace Appoint.Models
{
    public class AppContext
    {
        private List<AppointmentShortcut> _shortcuts;

        public IEnumerable<AppointmentShortcut> Shortcuts { get { return _shortcuts; } }
        public AppointmentShortcut GetShortcut(string kind)
        {
            var shortcut = _shortcuts.Find(s => s.Kind == kind);
            if (shortcut != null)
                return shortcut;
            else
                throw new ArgumentException("Unknown shortcut kind");
        }

        public AppContext()
        {
            Load();
        }

        private const string _AppointsFilename = "appoints.xml";

        public void Load()
        {
            using (var storage = IsolatedStorageFile.GetUserStoreForApplication())
            {
                if (storage.FileExists(_AppointsFilename))
                {
                    using (var file = storage.OpenFile(_AppointsFilename, System.IO.FileMode.Open))
                    {
                        try
                        {
                            var serializer = new XmlSerializer(typeof(List<string>));
                            var shortcutsEncoded = (List<string>)serializer.Deserialize(file);

                            _shortcuts = shortcutsEncoded.Select(s => AppointmentFactory.Create(s)).ToList();

                            AddNewShortcuts(_shortcuts);
                        }
                        catch (Exception)
                        {
                            _shortcuts = CreateDefaultShortcuts();
                        }
                    }
                }
                else
                {
                    _shortcuts = CreateDefaultShortcuts();
                }
            }

            if (_mainVM != null)
            {
                _mainVM.OnShortcutsChanged();
            }
        }

        public void Save()
        {
            using (var storage = IsolatedStorageFile.GetUserStoreForApplication())
            {
                if (storage.FileExists(_AppointsFilename))
                {
                    storage.DeleteFile(_AppointsFilename);
                }

                using (var file = storage.CreateFile(_AppointsFilename))
                {
                    var serializer = new XmlSerializer(typeof(List<string>));
                    serializer.Serialize(file, _shortcuts.Select(s => AppointmentFactory.ShortcutToParamsString(s)).ToList());
                }
            }
        }

        public void Pin(AppointmentShortcut shortcut)
        {
            try
            {
                var tileData = new StandardTileData
                {
                    Title = "",
                    BackContent = shortcut.Description,
                    BackgroundImage = new Uri(shortcut.TileImageUri, UriKind.Relative)
                };

                ShellTile.Create(
                    new Uri(String.Format("/LaunchPage.xaml?{0}", shortcut.Appointment.Params), UriKind.Relative),
                    tileData,
                    supportsWideTile: false);
            }
            catch (InvalidOperationException)
            {
                MessageBox.Show("This shortcut is already pinned!");
            }
        }

        private List<AppointmentShortcut> CreateDefaultShortcuts()
        {
            return AppointmentFactory.Kinds.All.Select(
                kind => AppointmentFactory.Default(kind)).ToList();
        }

        private void AddNewShortcuts(List<AppointmentShortcut> shortcuts)
        {
            var missing = AppointmentFactory.Kinds.All.Where(k => shortcuts.FirstOrDefault(s => s.Kind == k) == null);
            foreach (var kind in missing)
            {
                shortcuts.Add(AppointmentFactory.Default(kind));
            }
        }

        public void ResetToDefaultShortcuts()
        {
            _shortcuts = CreateDefaultShortcuts();
            Save();
            _mainVM.OnShortcutsChanged();
        }

        public void UpdateWeekEndsOn(int newValue)
        {
            foreach (var shortcut in _shortcuts)
            {
                var weekDate = shortcut.Appointment.Date as ThisWeekDate;
                if (weekDate != null)
                {
                    weekDate.WeekEndsOn = newValue;
                }
            }

            Save();
        }

        private MainViewModel _mainVM;
        public MainViewModel MainViewModel
        {
            get
            {
                if (_mainVM == null)
                {
                    _mainVM = new MainViewModel();
                }
                return _mainVM;
            }
        }
    }
}
