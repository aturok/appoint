﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Appoint.Resources;

namespace Appoint.Models
{
    public static class AppointmentFactory
    {
        public static class Kinds
        {
            public const string Today = TodayDate.Kind;
            public const string Tomorrow = TomorrowDate.Kind;
            public const string InDays = InDaysDate.Kind;
            public const string ThisWeek = ThisWeekDate.Kind;
            public const string NextWeek = NextWeekDate.Kind;
            public const string InAWeek = InAWeekDate.Kind;
            public const string InWeeks = InWeeksDate.Kind;
            public const string ThisMonth = ThisMonthDate.Kind;
            public const string NextMonth = NextMonthDate.Kind;
            public const string InAMonth = InAMonthDate.Kind;
            public const string InMonths = InMonthsDate.Kind;
            public const string Random = RandomDate.Kind;
            public const string Diamond = "diamond";

            public static readonly IEnumerable<string> All;

            static Kinds()
            {
                All = new List<string> 
                { 
                    Today,
                    Tomorrow,
                    InDays,
                    ThisWeek,
                    InAWeek,
                    NextWeek,
                    InWeeks,
                    ThisMonth,
                    InAMonth, 
                    NextMonth,
                    Random,
                    Diamond
                };
            }
        }

        public static AppointmentShortcut Create(string parameters)
        {
            var p = ParamsFromString(parameters);
            return Create(p);
        }

        public static AppointmentShortcut Create(IDictionary<string, string> parameters)
        {
            string kind = parameters["kind"];
            var shortcut = Default(kind);

            if (kind == Kinds.Diamond)
            {
                ((ShiftingShortcut)shortcut).ActiveKind = parameters["activekind"];
            }

            shortcut.Appointment.Date.FromParams(parameters);
            shortcut.Appointment.Time.FromParams(parameters);
            shortcut.Appointment.Duration.FromParams(parameters);

            return shortcut;
        }

        public static AppointmentShortcut Default(string kind)
        {
            if (kind == Kinds.Diamond)
            {
                var meta = new ShiftingShortcut
                {
                    Kind = kind,
                    Title = GetTitle(kind),
                    TileImageUri = GetTileImage(kind),
                    Appointment = new Appointment
                    {
                        Time = new Time { Hour = 21 }
                    },
                    DurationOptions = _DurationOptions
                };
                meta.ActiveKind = Kinds.Today;
                return meta;
            }

            var shortcut = new AppointmentShortcut
            {
                Kind = kind,
                Title = GetTitle(kind),
                TileImageUri = GetTileImage(kind),
                Appointment = new Appointment
                {
                    Time = new Time { Hour = 21 },
                    Date = GetDate(kind)
                },
                DurationOptions = _DurationOptions
            };

            var spanOptions = GetSpanOptions(kind);
            if (spanOptions != null)
            {
                shortcut.SpanOptions = spanOptions;
                shortcut.SpanMessage = GetSpanMessage(kind);
            }

            return shortcut;
        }

        private static string GetTitle(string kind)
        {
            switch (kind)
            {
                case Kinds.Today:
                    return "Today";
                case Kinds.Tomorrow:
                    return "Tomorrow";
                case Kinds.InDays:
                    return "In X Days";
                case Kinds.ThisWeek:
                    return "This Week";
                case Kinds.InAWeek:
                    return "In a Week";
                case Kinds.NextWeek:
                    return "Next Week";
                case Kinds.InWeeks:
                    return "In X Weeks";
                case Kinds.ThisMonth:
                    return "This Month";
                case Kinds.InAMonth:
                    return "In a Month";
                case Kinds.NextMonth:
                    return "Next Month";
                case Kinds.Random:
                    return "Random Date";
                case Kinds.Diamond:
                    return "Meta";
                default:
                    throw new ArgumentException(String.Format("Unknown shortcut kind: {0}", kind));
            }
        }

        private static BaseDate GetDate(string kind)
        {
            switch (kind)
            {
                case Kinds.Today:
                    return new TodayDate();
                case Kinds.Tomorrow:
                    return new TomorrowDate();
                case Kinds.InDays:
                    return new InDaysDate();
                case Kinds.ThisWeek:
                    return new ThisWeekDate() { WeekEndsOn = Settings.WeekEndsOn };
                case Kinds.InAWeek:
                    return new InAWeekDate();
                case Kinds.NextWeek:
                    return new NextWeekDate() { WeekEndsOn = Settings.WeekEndsOn };
                case Kinds.InWeeks:
                    return new InWeeksDate();
                case Kinds.ThisMonth:
                    return new ThisMonthDate();
                case Kinds.InAMonth:
                    return new InAMonthDate();
                case Kinds.NextMonth:
                    return new NextMonthDate();
                case Kinds.Random:
                    return new RandomDate();
                case Kinds.Diamond:
                    return new TodayDate();
                default:
                    throw new ArgumentException(String.Format("Unknown shortcut kind: {0}", kind));
            }
        }

        private static string GetTileImage(string kind)
        {
            return String.Format("/Assets/Tiles/{0}336.png", kind);
        }

        private static IEnumerable<SpanOption> GetSpanOptions(string kind)
        {
            switch (kind)
            {
                case Kinds.InDays:
                case Kinds.InWeeks:
                    return Enumerable.Range(2, 19).Select(i => new SpanOption { Title = i.ToString(), Value = i }).ToList();
                case Kinds.ThisWeek:
                case Kinds.NextWeek:
                    return new List<SpanOption>
                    {
                        new SpanOption { Title = "Monday", Value = 1 },
                        new SpanOption { Title = "Tuesday", Value = 2 },
                        new SpanOption { Title = "Wednesday", Value = 3 },
                        new SpanOption { Title = "Thursday", Value = 4 },
                        new SpanOption { Title = "Friday", Value = 5 },
                        new SpanOption { Title = "Saturday", Value = 6 },
                        new SpanOption { Title = "Sunday", Value = 0 }
                    };
                case Kinds.ThisMonth:
                case Kinds.NextMonth:
                    return new List<SpanOption> { new SpanOption { Title = "last", Value = 0 } }.Concat(
                        Enumerable.Range(1,10).Select(i => new SpanOption { Title = i.ToString(), Value = i })).ToList();
                case Kinds.Random:
                    return new int[] { 30, 50, 100, 365 }.Select(
                        s => new SpanOption { Title = s.ToString(), Value = s }).ToList();
                default:
                    return null;
            }
        }

        private static string GetSpanMessage(string kind)
        {
            switch (kind)
            {
                case Kinds.InDays:
                    return "days";
                case Kinds.InWeeks:
                    return "weeks";
                case Kinds.ThisWeek:
                case Kinds.NextWeek:
                    return "day";
                case Kinds.ThisMonth:
                case Kinds.NextMonth:
                    return "days before month end";
                case Kinds.Random:
                    return "days (not later than)";
                default:
                    return null;
            }
        }

        public static string ShortcutToParamsString(AppointmentShortcut shortcut)
        {
            if (shortcut.Kind == Kinds.Diamond)
                return ShiftingShortcutToParamsString(shortcut);
            else
                return shortcut.Appointment.Params;
        }

        private static string ShiftingShortcutToParamsString(AppointmentShortcut shortcut)
        {
            var paramsDict = ParamsFromString(shortcut.Appointment.Params);
            paramsDict["kind"] = Kinds.Diamond;
            paramsDict["activekind"] = ((ShiftingShortcut)shortcut).ActiveKind;
            return String.Join("&", paramsDict.Select(kv => String.Format("{0}={1}", kv.Key, kv.Value)));
        }

        public static Dictionary<string, string> ParamsFromString(string parameters)
        {
            return parameters.Split('&')
                        .Select(s => s.Split('='))
                        .ToDictionary(kv => kv[0].Trim(), kv => kv[1].Trim());
        }

        private static List<SpanOption> _DurationOptions;
        private static List<KindOption> _KindOptions;

        public static IEnumerable<KindOption> KindOptions { get { return _KindOptions; } }

        static AppointmentFactory()
        {
            _DurationOptions = new int[] { 0, 30, 60, 90, 120, -1 }.Select(d => CreateDurationOption(d)).ToList();
            _KindOptions = Kinds.All.Except(new string[] { Kinds.Diamond }).Select(
                k => new KindOption { Kind = k, Title = GetTitle(k) }).ToList();
        }

        private static SpanOption CreateDurationOption(int duration)
        {
            var option = new SpanOption { Value = duration };

            if (duration < 0)
            {
                option.Title = AppResources.DurationTillEndOfDay;
            }
            else if (duration < 60)
            {
                option.Title = String.Format(AppResources.TimeMinutesFormat, duration);
            }
            else if (duration % 60 == 0)
            {
                option.Title = String.Format(AppResources.TimeHoursFormat, duration / 60);
            }
            else
            {
                option.Title = String.Format(AppResources.TimeHoursMinutesFormat, duration / 60, duration % 60);
            }

            return option;
        }

    }
}
