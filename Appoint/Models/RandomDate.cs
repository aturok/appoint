﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Appoint.Models
{
    public class RandomDate : BaseDate
    {
        public const string Kind = "random";

        private Random _generator;

        public override string Description { get { return String.Format("on random date in less than {0} days", Span); } }

        public override DateTime GetDate(DateTime baseDate)
        {
            int plusDays = (int)(_generator.NextDouble() * Span);
            return baseDate.AddDays(plusDays);
        }

        public RandomDate(Random randomGen)
            : base(Kind)
        {
            _generator = randomGen;
            Span = 100;
        }

        public RandomDate()
            : this(new Random())
        {
        }
    }
}
