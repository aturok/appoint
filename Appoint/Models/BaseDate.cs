﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Appoint.Models
{
    public abstract class BaseDate
    {
        private string _kind;

        public virtual int Span { get; set; }
        public DateTime Date { get { return GetDate(DateTime.Today); } }
        public abstract string Description { get; }

        public abstract DateTime GetDate(DateTime baseDate);

        protected virtual void InitFromParams(IDictionary<string, string> p)
        {
            Span = Int32.Parse(p["relspan"]);
        }

        public virtual BaseDate FromParams(IDictionary<string, string> p)
        {
            InitFromParams(p);
            return this;
        }

        public virtual string Params { get { return string.Format("kind={0}&relspan={1}", _kind, Span); } }

        protected BaseDate(string kind)
        {
            _kind = kind;
        }
    }
}
