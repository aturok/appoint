﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Appoint.Models
{
    public class ThisMonthDate : BaseDate
    {
        public const string Kind = "thismonth";

        protected string Prefix
        {
            get
            {
                if (Span == 0)
                    return "end of";
                else if (Span == 1)
                    return "1 day before the end of";
                else
                    return String.Format("{0} days before the end of", Span);
            }
        }

        protected virtual string Suffix { get { return "month"; } }

        public override string Description
        {
            get 
            {
                return String.Format("{0} {1}", Prefix, Suffix);
            }
        }

        public override DateTime GetDate(DateTime baseDate)
        {
            var nextMonth = baseDate.AddMonths(1);
            var nextStart = new DateTime(nextMonth.Year, nextMonth.Month, 1);
            return nextStart.AddDays(-1 - Span) + baseDate.TimeOfDay;
        }

        public ThisMonthDate()
            : this(Kind)
        {
        }

        protected ThisMonthDate(string kind)
            : base(kind)
        {
        }
    }

    public class NextMonthDate : ThisMonthDate
    {
        public new const string Kind = "nextmonth";

        protected override string Suffix { get { return "next month"; } }

        public override DateTime GetDate(DateTime baseDate)
        {
            return base.GetDate(baseDate.AddMonths(1));
        }

        public NextMonthDate()
            : base(Kind)
        {
        }
    }
}
