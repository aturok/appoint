﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Appoint.Models
{
    public class SettingsViewModel
    {
        private IEnumerable<SpanOption> _weekEndsOptions;
        public IEnumerable<SpanOption> WeekEndsOptions { get { return _weekEndsOptions; } }

        public SpanOption WeekEndsOn { get; set; }

        public bool TapToCustomize { get; set; }

        public void Save()
        {
            var oldValue = Settings.WeekEndsOn;
            if (oldValue != WeekEndsOn.Value)
            {
                Settings.WeekEndsOn = WeekEndsOn.Value;
                App.Context.UpdateWeekEndsOn(WeekEndsOn.Value);
            }

            Settings.TapToCustomize = TapToCustomize;
        }

        public SettingsViewModel()
        {
            _weekEndsOptions = new List<SpanOption>
            {
                new SpanOption { Title = "Saturday", Value = (int)DayOfWeek.Saturday },
                new SpanOption { Title = "Sunday", Value = (int)DayOfWeek.Sunday },
            };
            int currentWeekEnds = Settings.WeekEndsOn;
            WeekEndsOn = _weekEndsOptions.FirstOrDefault(o => o.Value == currentWeekEnds);

            TapToCustomize = Settings.TapToCustomize;
        }
    }
}
