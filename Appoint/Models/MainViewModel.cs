﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace Appoint.Models
{
    public class MainViewModel : INotifyPropertyChanged
    {
        public IEnumerable<AppointmentShortcut> Shortcuts { get; set; }

        public MainViewModel()
        {
            OnShortcutsChanged();
        }

        public void OnShortcutsChanged()
        {
            Shortcuts = App.Context.Shortcuts.ToList();

            if (PropertyChanged != null)
            {
                PropertyChanged.Invoke(this, new PropertyChangedEventArgs("Shortcuts"));
            }
        }


        public event PropertyChangedEventHandler PropertyChanged;
    }
}
