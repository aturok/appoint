﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;

namespace Appoint.Models
{
    public class ThisWeekDate : BaseDate
    {
        public const string Kind = "thisweek";

        public const string WeekEndsOnKey = "WeekEndsOn";

        public int WeekEndsOn { get; set; }

        protected string GetDayOfWeekName(DateTime date)
        {
            return date.ToString("dddd", new CultureInfo("en-US"));
        }

        public override string Description
        {
            get { return String.Format("this {0}", GetDayOfWeekName(Date)); }
        }

        public override DateTime GetDate(DateTime baseDate)
        {
            int weekPenny = 0;
            for (int i = 0; i < 7; i++)
            {
                var d = baseDate.AddDays(i);
                if ((int)d.DayOfWeek == Span)
                {
                    return d.AddDays(7 * weekPenny).Date;
                }
                else if ((int)d.DayOfWeek == WeekEndsOn)
                {
                    weekPenny = -1;
                }
            }

            return baseDate;
        }

        public override string Params
        {
            get
            {
                return base.Params + String.Format("&{0}={1}",WeekEndsOnKey,WeekEndsOn.ToString());
            }
        }

        public override BaseDate FromParams(IDictionary<string, string> p)
        {
            base.FromParams(p);

            if (p.ContainsKey(WeekEndsOnKey))
            {
                WeekEndsOn = Int32.Parse(p[WeekEndsOnKey]);
            }

            return this;
        }

        public ThisWeekDate()
            : this(Kind)
        {
        }

        public ThisWeekDate(string kind)
            : base(kind)
        {
            Span = (int)DayOfWeek.Saturday;
            WeekEndsOn = (int)DayOfWeek.Saturday;
        }
    }

    public class NextWeekDate : ThisWeekDate
    {
        public new const string Kind = "nextweek";

        public override DateTime GetDate(DateTime baseDate)
        {
            return base.GetDate(baseDate).AddDays(7);
        }

        public override string Description
        {
            get
            {
                return String.Format("next {0}", GetDayOfWeekName(Date));
            }
        }

        public NextWeekDate()
            : base(Kind)
        {
        }
    }
}
