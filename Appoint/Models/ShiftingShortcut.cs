﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Appoint.Models
{
    public class KindOption
    {
        public string Kind { get; set; }
        public string Title { get; set; }
    }

    public class ShiftingShortcut : AppointmentShortcut
    {
        public IEnumerable<KindOption> KindOptions { get { return AppointmentFactory.KindOptions; } }

        private string _activeKind;
        public string ActiveKind
        {
            get { return _activeKind; }
            set
            {
                _activeKind = value;
                CopyFrom(AppointmentFactory.Default(value));
                OnKindChanged();
            }
        }

        private KindOption _selectedKind;

        public KindOption SelectedKind
        {
            get
            {
                if (_selectedKind == null)
                {
                    _selectedKind = KindOptions.First(o => o.Kind == ActiveKind);
                }
                return _selectedKind;
            }
            set
            {
                _selectedKind = value;
                ActiveKind = _selectedKind.Kind;
            }
        }

        private void CopyFrom(AppointmentShortcut shortcut)
        {
            this.Appointment.Date = shortcut.Appointment.Date;
            this.SpanOptions = shortcut.SpanOptions;
            this.SpanMessage = shortcut.SpanMessage;
            ChangeSpan(shortcut.SelectedSpan);
        }

        private void OnKindChanged()
        {
            OnPropertiesChanged(
                "Description",
                "AppointmentDate",
                "IsSpanCustomizable",
                "SpanOptions",
                "SelectedSpan",
                "SpanMessage");
        }
    }
}
