﻿using System;
using Microsoft.Phone.Controls;
using Windows.ApplicationModel.Store;

using Appoint.Models;

namespace Appoint.Utility
{
    public class Navigator
    {
        public static void ToCustomization(PhoneApplicationPage page, AppointmentShortcut shortcut)
        {
            if (shortcut.Kind == AppointmentFactory.Kinds.Diamond && Products.IsDiamondAcquired == false)
            {
                Navigator.ToBuyDiamond(page);
            }
            else
            {
                page.NavigationService.Navigate(GetCustomizationUri(shortcut.Kind));
            }
        }

        public static void ToCalendar(PhoneApplicationPage page, AppointmentShortcut shortcut)
        {
            if (shortcut.Kind == AppointmentFactory.Kinds.Diamond && Products.IsDiamondAcquired == false)
            {
                Navigator.ToBuyDiamond(page);
            }
            else
            {
                shortcut.Appointment.Task.Show();
            }
        }

        public static void PinShortcut(PhoneApplicationPage page, AppointmentShortcut shortcut)
        {
            if (shortcut.Kind == AppointmentFactory.Kinds.Diamond && Products.IsDiamondAcquired == false)
            {
                Navigator.ToBuyDiamond(page);
            }
            else
            {
                App.Context.Pin(shortcut);
            }
        }

        public static void ShortcutTapped(PhoneApplicationPage page, AppointmentShortcut shortcut)
        {
            if (Appoint.Models.Settings.TapToCustomize)
            {
                ToCustomization(page, shortcut);
            }
            else
            {
                ToCalendar(page, shortcut);
            }
        }

        public static void ToBuyDiamond(PhoneApplicationPage page)
        {
            page.NavigationService.Navigate(new Uri("/BuyDiamondPage.xaml", UriKind.Relative));
        }

        private static Uri GetCustomizationUri(string kind)
        {
            string uriString = null;
            if (kind == AppointmentFactory.Kinds.Diamond)
            {
                uriString = String.Format("/CustomizeDiamondPage.xaml?kind={0}", kind);
            }
            else
            {
                uriString = String.Format("/CustomizePage.xaml?kind={0}", kind);
            }

            return new Uri(uriString, UriKind.Relative);
        }
    }
}
