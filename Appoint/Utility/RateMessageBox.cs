﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Windows.Controls;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Tasks;

using Appoint.Resources;

namespace Appoint.Utility
{
    public class RateMessageBox
    {
        private CustomMessageBox _messageBox;
        private CheckBox _dontShowCheckBox;

        public RateMessageBox()
        {
            _dontShowCheckBox = new CheckBox
            {
                IsChecked = false,
                Content = new TextBlock
                {
                    Text = AppResources.RateDialogDontShowText,
                    TextWrapping = System.Windows.TextWrapping.Wrap
                }
            };

            _messageBox = new CustomMessageBox
            {
                Caption = AppResources.RateDialogCaption,
                Message = AppResources.RateDialogMessage,
                LeftButtonContent = AppResources.RateDialogRateButtonText,
                RightButtonContent = AppResources.RateDialogDontRateButtonText,
                Content = _dontShowCheckBox
            };
        }

        public void Show()
        {
            _messageBox.Dismissed += DialogClosed;
            _messageBox.Show();
        }

        public void DialogClosed(object sender, DismissedEventArgs e)
        {
            _messageBox.Dismissed -= DialogClosed;

            if (_dontShowCheckBox.IsChecked == true)
                OnDontShowChecked();

            if (e.Result == CustomMessageBoxResult.LeftButton)
                OnRateButtonPressed();
            else if (e.Result == CustomMessageBoxResult.RightButton)
                OnCancelButtonPressed();
        }

        protected virtual void OnRateButtonPressed()
        {
            var rateTask = new MarketplaceReviewTask();
            rateTask.Show();
        }

        protected virtual void OnCancelButtonPressed()
        {
        }

        protected virtual void OnDontShowChecked()
        {
            Appoint.Models.Settings.DontAskForRating = false;
        }
    }
}
