﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Appoint.Resources;
using Appoint.Utility;
using Appoint.Models;

namespace Appoint
{
    public partial class MainPage : PhoneApplicationPage
    {
        protected Models.MainViewModel _vm;
        private bool _askForRating;
        private bool _showHelp;

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            if (_askForRating == true)
            {
                _askForRating = false;
                var rater = new Utility.RateMessageBox();
                rater.Show();
            }

            if (_showHelp)
            {
                _showHelp = false;
                HelpPane.Visibility = System.Windows.Visibility.Visible;
            }
            else
            {
                HelpPane.Visibility = System.Windows.Visibility.Collapsed;
            }
        }

        protected override void OnBackKeyPress(System.ComponentModel.CancelEventArgs e)
        {
            if (HelpPane.Visibility == System.Windows.Visibility.Visible)
            {
                HelpPane.Visibility = System.Windows.Visibility.Collapsed;
                e.Cancel = true;
            }
            
            base.OnBackKeyPress(e);
        }

        public MainPage()
        {
            _vm = App.Context.MainViewModel;
            DataContext = _vm;

            _askForRating = Appoint.Models.Settings.IsTimeToAskForRating;
            _showHelp = Models.Settings.LaunchCount == 1;

            ApplicationBar = CreateAppBar();

            InitializeComponent();
        }

        private void ShortcutControl_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            Navigator.ShortcutTapped(this, GetShortcut(sender));
        }

        private void Customize_Click(object sender, RoutedEventArgs e)
        {
            Utility.Navigator.ToCustomization(this, GetShortcut(sender));
        }

        private void Pin_Click(object sender, RoutedEventArgs e)
        {
            Navigator.PinShortcut(this, GetShortcut(sender));
        }

        private void Appoint_Click(object sender, RoutedEventArgs e)
        {
            Utility.Navigator.ToCalendar(this, GetShortcut(sender));
        }

        public static AppointmentShortcut GetShortcut(object control)
        {
            var element = (FrameworkElement)control;
            return (AppointmentShortcut)element.DataContext;
        }

        private void ShortcutContextMenu_Unloaded(object sender, RoutedEventArgs e)
        {
            var menu = sender as ContextMenu;
            menu.ClearValue(FrameworkElement.DataContextProperty);
        }

        private ApplicationBar CreateAppBar()
        {
            var bar = new ApplicationBar();

            var settingsItem = new ApplicationBarMenuItem("settings");
            settingsItem.Click += SettingsItem_Click;
            bar.MenuItems.Add(settingsItem);

            var aboutItem = new ApplicationBarMenuItem("about");
            aboutItem.Click += AboutItem_Click;
            bar.MenuItems.Add(aboutItem);

            var helpItem = new ApplicationBarMenuItem("help");
            helpItem.Click += HelpItem_Click;
            bar.MenuItems.Add(helpItem);

            bar.Mode = ApplicationBarMode.Minimized;

            return bar;
        }

        public void SettingsItem_Click(object sender, EventArgs e)
        {
            NavigationService.Navigate(new Uri("/SettingsPage.xaml", UriKind.Relative));
        }

        public void AboutItem_Click(object sender, EventArgs e)
        {
            NavigationService.Navigate(new Uri("/AboutPage.xaml", UriKind.Relative));
        }

        public void HelpItem_Click(object sender, EventArgs e)
        {
            HelpPane.Visibility = System.Windows.Visibility.Visible;
        }

        private void CloseHelp_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            HelpPane.Visibility = System.Windows.Visibility.Collapsed;
        }

        private void MoreHelpText_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            var mailTask = new Microsoft.Phone.Tasks.EmailComposeTask
            {
                To = AppResources.SuggestionsEmailAddress,
                Subject = AppResources.HelpEmailSubject
            };
            mailTask.Show();
        }
    }
}